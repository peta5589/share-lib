def call(Map project) {

    // Run application process to deploy file
    stage("Urban-Code Deploy on ${project.environment}") {

        requestID = sh(
                returnStdout: true,
                script: "curl --silent -k -u PasswordIsAuthToken:{'token':${env.UCD_TOKEN}} ${project.ucdUrl} -X PUT -d " + "\"{" +
                        "'application': ${project.appName}," +
                        "'applicationProcess': ${project.appProcess}," +
                        "'environment': ${project.environment}," +
                        "'onlyChanged': ${project.onlyChanged}," +
                        "'versions': [{'version': ${project.version}, 'component': '${project.artifactId}'}] }\""
        ).split('"')[3]

        while (status == "CLOSED") {
            sh "sleep 10"
            ucdResult = sh(
                    returnStdout: true,
                    script: "curl -k -u PasswordIsAuthToken:{'token':${env.UCD_TOKEN}} ${project.ucdUrl}Status?request=$requestID"
            )
            ucdResult.trim()
            //check if it works like that
            status = ucdResult.split('"')[3]
            result = ucdResult.split('"')[7]
        }

        if (result.trim() != "SUCCEEDED") {
            currentBuild.result = 'ABORTED'
            error('UCD process failed')
        }
    }
}


//        //Job parameters
//        parameters {
//            string(defaultValue: "FENIX_BI_API", description: 'Application name:', name: 'APPLICATION')
//            string(defaultValue: "TOMCAT_APPLICATION_TEMPLATE_DeployWAR", description: 'Application process:', name: 'APP_PROCESS')
//            string(defaultValue: "DEV", description: 'Application enviroment', name: 'APP_ENVIRONMENT')
//            string(defaultValue: "false", description: 'Use only changed versions?', name: 'ONLY_CHANGED')
//            string(defaultValue: "2.1", description: 'Component version:', name: 'COMPONENT_VERSION')
//        }
//
//        //Environment properties
//        environment {
//            PART_URL = 'Status?request='
//            HOST = 'https://ucd.kb.cz/cli/applicationProcessRequest/request'
//            COMPONENTS = "{'version': ${params.COMPONENT_VERSION}, 'component': 'bi-api-audit'}," +
//                    "{'version': ${params.COMPONENT_VERSION}, 'component': 'bi-api-blank'}," +
//                    "{'version': ${params.COMPONENT_VERSION}, 'component': 'bi-api-incident'}," +
//                    "{'version': ${params.COMPONENT_VERSION}, 'component': 'bi-api-jira'}," +
//                    "{'version': ${params.COMPONENT_VERSION}, 'component': 'bi-api-mail'}," +
//                    "{'version': ${params.COMPONENT_VERSION}, 'component': 'bi-api-monitor'}," +
//                    "{'version': ${params.COMPONENT_VERSION}, 'component': 'bi-api-mwf'}," +
//                    "{'version': ${params.COMPONENT_VERSION}, 'component': 'bi-api-odis'}," +
//                    "{'version': ${params.COMPONENT_VERSION}, 'component': 'bi-api-proxy'}," +
//                    "{'version': ${params.COMPONENT_VERSION}, 'component': 'bi-api-script'}," +
//                    "{'version': ${params.COMPONENT_VERSION}, 'component': 'bi-api-setting-store'}," +
//                    "{'version': ${params.COMPONENT_VERSION}, 'component': 'bi-api-teradata'}," +
//                    "{'version': ${params.COMPONENT_VERSION}, 'component': 'bi-api-wfdoc'}," +
//                    "{'version': ${params.COMPONENT_VERSION}, 'component': 'bi-api-wfgen'}," +
//                    "{'version': ${params.COMPONENT_VERSION}, 'component': 'bi-api-wf-monitor-di'}," +
//                    "{'version': ${params.COMPONENT_VERSION}, 'component': 'bi-api-wf-monitor-kb'}," +
//                    "{'version': ${params.COMPONENT_VERSION}, 'component': 'bi-api-branch'}," +
//                    "{'version': ${params.COMPONENT_VERSION}, 'component': 'bi-api-graphviz'}," +
//                    "{'version': ${params.COMPONENT_VERSION}, 'component': 'bi-api-sqltransform'}"
//        }
