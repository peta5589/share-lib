def call(Map project){
    stage('Feature: Set qualifier to default value') {
        sh "curl --silent -k -u PasswordIsAuthToken:{'token':${env.UCD_TOKEN}} ${project.ucdUrl} -X PUT -d '{'" +
                "'application': ${project.application}," +
                "'environment': ${project.environment}," +
                "'name': ${project.name}," +
                "'value': ${project.qualifier} }'"

        println "Environment property qualifier has been set to null"
    }
}
