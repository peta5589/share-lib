def call() {
    pipeline {
        agent any
        tools { maven "maven" }

        stages {
            stage('Main') {
                steps {
                    script {
                        stage("Remove and deploy") {
                            sh "git clone http://${BITBUCKET_USER}:${BITBUCKET_PASSWORD}@localhost:7990/scm/ter/main.git"

                            date = sh(script: "date '+%b %d'", returnStdout: true)
                            dir('main') {
                                directories = sh(script: "ls -ld * | grep '${date}' | cut -d :  -f2 | cut -d ' ' -f2", returnStdout: true).split("\\r?\\n")
                                directories.each {
                                    dir(it) {
                                        sh "mvn test"
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        post {
            always {
                echo 'One way or another, I have finished'
                deleteDir()
            }
            success {
                mail bcc: '', body: "${NAME} success", cc: '', from: 'kb.teradata@gmail.com', replyTo: '', subject: 'Success', to: 'sima.petr85@gmail.com'
                echo 'I succeeded!'
            }
            unstable {
                mail bcc: '', body: "${NAME} unstable", cc: '', from: 'kb.teradata@gmail.com', replyTo: '', subject: 'Unstable', to: 'sima.petr85@gmail.com'
                echo 'I am unstable :/'
            }
            failure {
                mail bcc: '', body: "${NAME} failure", cc: '', from: 'kb.teradata@gmail.com', replyTo: '', subject: 'Failure', to: 'sima.petr85@gmail.com'
                echo 'I failed 😞'
            }
            changed {
                mail bcc: '', body: "${NAME} changed", cc: '', from: 'kb.teradata@gmail.com', replyTo: '', subject: 'Changed', to: 'sima.petr85@gmail.com'
                echo 'Things were different before...'
            }
        }
    }
}
