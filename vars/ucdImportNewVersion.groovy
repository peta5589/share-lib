def call (Map project) {
    stage('Urban-Code Create new version'){

        requestID = sh(
                returnStdout: true,
                script: "curl --silent -k -u PasswordIsAuthToken:{'token':${env.UCD_TOKEN}} ${project.ucdURL} -X PUT -d '{" +
                        " 'application':${project.application}, " +
                        " 'applicationProcess':${project.applicationProcess}," +
                        " 'environment': ${project.environment}, " +
                        " 'onlyChanged': ${project.onlyChanged}, " +
                        " 'properties': {" +
                        "       'artifactId': ${project.artifactId}, " +
                        "       'versionName': ${project.version}" +
                        "  }" +
                        "}" ).split('"')[3]

        while (status == "CLOSED") {
            sh "sleep 10"
            ucdResult = sh(
                    returnStdout: true,
                    script: "curl -k -u PasswordIsAuthToken:{'token':${env.UCD_TOKEN}} ${project.ucdUrl}Status?request=$requestID"
            )
            ucdResult.trim()
            //check if it works like that
            status = ucdResult.split('"')[3]
            result = ucdResult.split('"')[7]
        }

        if (result.trim() != "SUCCEEDED") {
            currentBuild.result = 'ABORTED'
            error('UCD process failed')
        }
    }
}