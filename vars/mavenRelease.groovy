def call(Map project) {
    stage('Master: Release version') {
        sh "mvn -B -P ${project.profile} release:prepare release:perform -Darguments='-DskipTests=true' -Dmaven.javadoc.skip=true"
    }
}
