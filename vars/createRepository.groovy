def call() {
    pipeline {
        agent any
        tools { maven "maven" }

        stages {
            stage('Main') {
                steps {
                    script {
                        stage("Create repo") {
                            sh "echo create repo ${REPO_NAME}"

                            sh "curl -H \"Content-type: application/json\" -u ${BITBUCKET_USER}:${BITBUCKET_PASSWORD} -d '{\n" +
                                    "    \"slug\": \"${REPO_NAME}\",\n" +
                                    "    \"name\": \"${REPO_NAME}\",\n" +
                                    "    \"project\": {\n" +
                                    "        \"key\": \"MIC\"\n" +
                                    "    }\n" +
                                    "}' 'http://localhost:7990/rest/api/1.0/projects/TER/repos/'"
                        }

                        stage("Add web-hook") {
                            sh "echo create web-hook of ${REPO_NAME}"

                            sh "curl -XPUT -H \"Content-type: application/json\" -u ${BITBUCKET_USER}:${BITBUCKET_PASSWORD} -d '{\n" +
                                    "  \"version\": \"3\",\n" +
                                    "  \"locationCount\": \"2\",\n" +
                                    "  \"httpMethod\": \"POST\",\n" +
                                    "  \"url\": \"http://localhost:8080/job/validate-release/buildWithParameters?NAME=\${repository.name}\",\n" +
                                    "  \"skipSsl\": true,\n" +
                                    "  \"useAuth\": true,\n" +
                                    "  \"user\": \"${BITBUCKET_USER}\",\n" +
                                    "  \"pass\": \"${BITBUCKET_PASSWORD}\",\n" +
                                    "  \"postContentType\": \"application/json\",\n" +
                                    "  \"postData\": \"\",\n" +
                                    "  \"branchFilter\": \"^release\$\",\n" +
                                    "  \"tagFilter\": \"\",\n" +
                                    "  \"userFilter\": \"\",\n" +
                                    "  \"httpMethod2\": \"POST\",\n" +
                                    "  \"url2\": \"http://localhost:8080/job/validate-test/buildWithParameters?NAME=\${repository.name}\",\n" +
                                    "  \"skipSsl2\": true,\n" +
                                    "  \"useAuth2\": true,\n" +
                                    "  \"user2\": \"${BITBUCKET_USER}\",\n" +
                                    "  \"pass2\": \"${BITBUCKET_PASSWORD}\",\n" +
                                    "  \"postContentType2\": \"application/json\",\n" +
                                    "  \"postData2\": \"\",\n" +
                                    "  \"branchFilter2\": \"^test\$\",\n" +
                                    "  \"tagFilter2\": \"\",\n" +
                                    "  \"userFilter2\": \"\"\n" +
                                    "}' 'http://localhost:7990/rest/api/1.0/projects/TER/repos/${REPO_NAME}/settings/hooks/de.aeffle.stash.plugin.stash-http-get-post-receive-hook:http-get-post-receive-hook/enabled'"
                        }

                        stage("Install archetype to jenkins (temporary)") {
                            sh "echo prepare libraries"

                            sh "git clone http://${BITBUCKET_USER}:${BITBUCKET_PASSWORD}@localhost:7990/scm/lib/archetype.git"
                            dir('archetype') {
                                sh "ls -alt"
                                sh "mvn install:install-file " +
                                        "-Dfile=./sql-validator-1.0-SNAPSHOT.jar " +
                                        "-DgroupId=cz.kb.autosdt " +
                                        "-DartifactId=sql-validator " +
                                        "-Dversion=1.0-SNAPSHOT " +
                                        "-Dpackaging=jar"
                            }
                            sh "rm -rf archetype"
                        }

                        stage("Generate archetype in the repository") {
                            sh "echo generate archetype"

                            sh "git clone http://${BITBUCKET_USER}:${BITBUCKET_PASSWORD}@localhost:7990/scm/ter/${REPO_NAME}.git"
                            sh "mvn archetype:generate -DarchetypeGroupId=cz.kb.autosdt " +
                                    "-DarchetypeArtifactId=sql-validator " +
                                    "-DarchetypeVersion=1.0-SNAPSHOT " +
                                    "-DgroupId=cz.kb.sql-validator-test " +
                                    "-DartifactId=${REPO_NAME} " +
                                    "-Dversion=1.0-SNAPSHOT " +
                                    "-DinteractiveMode=false " +
                                    "-DpomVersion=1.0-SNAPSHOT " +
                                    "-DpomGroupId=cz.kb.autosdt"

                            sh "git config --global user.email 'jenkins@example.com'"
                            sh "git config --global user.name 'jenkins'"

                            dir(REPO_NAME) {
                                sh "git add . && git status && git commit -m 'init'"
                                sh "git push http://${BITBUCKET_USER}:${BITBUCKET_PASSWORD}@localhost:7990/scm/ter/${REPO_NAME}.git"
                            }
                        }
                    }
                }
            }
        }
        post {
            always {
                echo 'One way or another, I have finished'
                deleteDir() /* clean up our workspace */
            }
            success {
                echo 'I succeeeded!'
            }
            unstable {
                echo 'I am unstable :/'
            }
            failure {
                echo 'I failed 😞'
            }
            changed {
                echo 'Things were different before...'
            }
        }
    }
}