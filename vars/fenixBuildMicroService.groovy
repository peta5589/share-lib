def call() {
    pipeline {
        agent any

        tools {
            jdk 'jdk1.8.0_101'
            maven 'maven 3'
            //maven 'fenix_maven'
        }

        parameters {
            booleanParam(
                    name: 'DEPLOY_FEATURE', defaultValue: false, description: 'Deploy feature-branch with classifier to DEV?')

            booleanParam(
                     name: 'DEPLOY_DEVELOP', defaultValue: false, description: 'Deploy develop-branch to TEST?')

            booleanParam(
                    name: 'DEPLOY_MASTER', defaultValue: false, description: 'Deploy Master-branch to REF?')

            booleanParam(
                    name: 'RELEASE_MASTER', defaultValue: true, description: 'Release Master?')
        }

        stages {
            stage('Main stage') {
                steps {
                    script {
                        gitCommitMSG =  sh( returnStdout: true, script: 'git log -1 --pretty=%B')

                        if (gitCommitMSG == "Skip build" || gitCommitMSG == "[maven-release-plugin]"){
                            skipBuild()
                        } else {
                            stage("Prepare build variables") {
                                appName = "FENIX_BI_API"
                                appProcess = "TOMCAT_APPLICATION_TEMPLATE_DeployWAR"
                                onlyChanged = "false"
                                projectName = 'bi-api'
                                projectKey = 'BISDC:bi-api'
                                javaHome = '/usr/java/jdk1.8.0_144'
                                javaVersion = '1.8'
                                language = 'java'
                                sonarBinaries = 'target'
                                sonarUrl = 'http://sonar.kb.cz:9007/sonar-central'
                                environment = "DEV"
                                propName = "qualifier"
                                ucdUrl = 'https://ucd.kb.cz/cli/environment/propValue'
                                branch = "${env.BRANCH_NAME}".split("/").last()
                                svnURL='https://svn2.kb.cz/svn/kb_deploy/UCD_PROJECTS/DEVELOPMENT/FENIX'
                                svnDir='FENIX'
                                pom = readMavenPom file: 'pom.xml'
                                version = pom.version
                                artifactId = pom.artifactId
                            }

                            if (env.BRANCH_NAME == 'master') {
                                buildAndDeployMasterBranch(
                                    svnURL: svnURL,
                                    svnDir: svnDir,
                                    appName: appName,
                                    ucdURL: ucdURL,
                                    appProcess: appProcess,
                                    artifactId: artifactId,
                                    onlyChanged: onlyChanged,
                                    version: version,
                                    environment: environment,
                                    releaseMaster: params.RELEASE_MASTER,
                                    deployMasterToRef: params.DEPLOY_MASTER)

                            } else {
                                mavenAndSonarTest(
                                        hostUrl: sonarUrl,
                                        login: "${env.SONAR_AUTH_TOKEN}",
                                        branchName: "${env.BRANCH_NAME}",
                                        projectName: projectName,
                                        projectKey: projectKey,
                                        javaSource: javaVersion,
                                        javaBinaries: sonarBinaries,
                                        language: language)
                            }

                            if (params.DEPLOY_FEATURE == true && env.BRANCH_NAME != 'develop') {
                                buildAndDeployFeatureBranch(
                                        name: propName,
                                        branch: branch,
                                        ucdUrl: ucdUrl,
                                        application: appName,
                                        onlyChanged: onlyChanged,
                                        appProcess: appProcess,
                                        environment: environment)
                            }

                            if (env.BRANCH_NAME == 'develop') {
                                buildAndDeployDevelopBranch(
                                        name: propName,
                                        branch: branch,
                                        ucdUrl: ucdUrl,
                                        application: appName,
                                        onlyChanged: onlyChanged,
                                        appProcess: appProcess,
                                        deployDevelopToTest: params.DEPLOY_DEVELOP,
                                        environment: environment)
                            }
                        }
                    }
                }
            }
        }

        post {

            success {
                echo 'I succeeded!'
//            script {
//                if (env.BRANCH_NAME != 'develop' && env.BRANCH_NAME != 'master') {
//                    if (params.DEPLOY_FEATURE_TO_DEV) {
//                        // Run smoke tests with devel profile on DEV server
//                        stage('testing1') {
//                            build job: '../AUTOMATIC_TESTING', wait: false, parameters: [
//                                    [$class: 'StringParameterValue', name: 'ENV_PROFILE', value: "devel"],
//                                    [$class: 'StringParameterValue', name: 'TYPE_TEST', value: "smoke"]
//                            ]
//                        }
//                    }
//                }
//
//                if (env.BRANCH_NAME == 'develop') {
//                    if (params.DEPLOY_DEVELOP_TO_TEST) {
//                        // Run smoke tests with devel profile on DEV server
//                        stage('testing2') {
//                            build job: '../AUTOMATIC_TESTING', wait: false, parameters: [
//                                    [$class: 'StringParameterValue', name: 'ENV_PROFILE', value: "devel"],
//                                    [$class: 'StringParameterValue', name: 'TYPE_TEST', value: "smoke"]
//                            ]
//                        }
//                    } else {
//                        // Run integration tests with devel profile on TEST server
//                        stage('testing3') {
//                            build job: '../AUTOMATIC_TESTING', wait: false, parameters: [
//                                    [$class: 'StringParameterValue', name: 'ENV_PROFILE', value: "devel"],
//                                    [$class: 'StringParameterValue', name: 'TYPE_TEST', value: "it"]
//                            ]
//                        }
//                        // Run e2e tests with devel profile on TEST server
//                        stage('testing4') {
//                            build job: '../AUTOMATIC_TESTING', wait: false, parameters: [
//                                    [$class: 'StringParameterValue', name: 'ENV_PROFILE', value: "devel"],
//                                    [$class: 'StringParameterValue', name: 'TYPE_TEST', value: "e2e"]
//                            ]
//                        }
//                    }
//                }
//
//                if (env.BRANCH_NAME == "master") {
//                    if (params.DEPLOY_MASTER_TO_REF) {
//                        // Run smoke tests with prod profile on REF server
//                        stage('testing5') {
//                            build job: '../AUTOMATIC_TESTING', wait: false, parameters: [
//                                    [$class: 'StringParameterValue', name: 'ENV_PROFILE', value: "prod"],
//                                    [$class: 'StringParameterValue', name: 'TYPE_TEST', value: "smoke"]
//                            ]
//                        }
//                    } else {
//                        // Run integration tests with devel profile on TEST server
//                        stage('testing6') {
//                            build job: '../AUTOMATIC_TESTING', wait: false, parameters: [
//                                    [$class: 'StringParameterValue', name: 'ENV_PROFILE', value: "devel"],
//                                    [$class: 'StringParameterValue', name: 'TYPE_TEST', value: "it"]
//                            ]
//                        }
//                        // Run e2e tests with devel profile on TEST server
//                        stage('testing7') {
//                            build job: '../AUTOMATIC_TESTING', wait: false, parameters: [
//                                    [$class: 'StringParameterValue', name: 'ENV_PROFILE', value: "devel"],
//                                    [$class: 'StringParameterValue', name: 'TYPE_TEST', value: "e2e"]
//                            ]
//                        }
//                    }
//                }
//            }
            }
            unstable {
                echo 'I am unstable :/'
            }
            failure {
                step(
                        [$class                  : 'Mailer',
                         notifyEveryUnstableBuild: true,
                         recipients              : emailextrecipients([
                                 [$class: 'CulpritsRecipientProvider'],
                                 [$class: 'RequesterRecipientProvider']
                         ]),
                         sendToIndividuals       : true]
                )
                echo 'I failed :('
            }
            always {
                echo 'One way or another, I have finished'
                deleteDir()
            }
        }
    }
}
