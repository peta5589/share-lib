def call(Map project) {

    environment = mavenDeploy(project.deployDevelopToTest)

    urbanCodeDeploy(
            onlyChanged: project.onlyChanged,
            appProcess: project.appProcess,
            application: project.application,
            ucdUrl: project.ucdUrl,
            environment: environment,
            version: pom.version,
            name: project.name)
}
