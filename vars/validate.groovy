def call() {
    pipeline {
        agent any
        tools { maven "maven" }

        stages {
            stage('Main') {
                steps {
                    script {
                        stage("Clone repository ${NAME}") {
                            sh "git clone http://${BITBUCKET_USER}:${BITBUCKET_PASSWORD}@localhost:7990/scm/ter/${NAME}.git"
                            dir(NAME) {
                                sh "git checkout test"
                                lastCommiterEmail = sh(script: "git show -s --format=\"%ae\" `git rev-parse --short HEAD`", returnStdout: true)
                            }
                            sh "echo ${lastCommiterEmail}"
                        }

                        stage("Install plugin to jenkins repository (temporary)") {
                            sh "git clone https://peta5589@bitbucket.org/peta5589/validate-plugin.git"

                            dir('validate-plugin') {
                                sh "mvn install:install-file " +
                                        "-Dfile=./validate-maven-plugin-1.0-SNAPSHOT.JAR " +
                                        "-DgroupId=cz.kb.td " +
                                        "-DartifactId=validate-maven-plugin " +
                                        "-Dversion=1.0-SNAPSHOT " +
                                        "-Dpackaging=jar"
                            }
                            sh "rm -rf validate-plugin"
                        }

                        stage("Validate maven") {
                            sh "echo validate maven"
                            dir(NAME) {
                                sh "mvn package -l output.log"
                            }
                        }
                    }
                }
            }
        }
        post {
            always {
                echo 'One way or another, I have finished'
                deleteDir()
            }
            success {
                emailext attachmentsPattern: '**/output.log', body: '${NAME} success', subject: 'test', to: lastCommiterEmail
                echo 'I succeeded!'
            }
            unstable {
                emailext attachmentsPattern: '**/output.log', body: '${NAME} unstable', subject: 'test', to: lastCommiterEmail
                echo 'I am unstable :/'
            }
            failure {
                result = sh(script: "sed -e '0,/--BEGIN--/d' -e '0,/--END--/!d' -e '/--END--/d' **/output.log, returnStdout: true)
                println result
                emailext body: result, subject: '${NAME} result', to: lastCommiterEmail
                echo 'I failed 😞'
            }

            changed {
                emailext attachmentsPattern: '**/output.log', body: '${NAME} changed', subject: 'test', to: lastCommiterEmail
                echo 'Things were different before...'
            }
        }
    }
}
