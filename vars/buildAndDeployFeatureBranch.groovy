def call(Map project) {

    deployWithQualifier(branch: project.branch)

    setQualifier(
        ucdUrl: project.ucdUrl,
        application: project.application,
        environment: project.environment,
        qualifier: "-${project.branch}")

    urbanCodeDeploy(
            onlyChanged: project.onlyChanged,
            appProcess: project.appProcess,
            application: project.application,
            ucdUrl: project.ucdUrl,
            environment: project.environment,
            version: project.version,
            name: project.name)

    setQualifier(
        ucdUrl: project.ucdUrl,
        application: project.application,
        environment: project.environment,
        qualifier: " ")
}
