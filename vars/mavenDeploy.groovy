def call (deploy) {

    if (deploy) {
        env = "TEST"

        stage('Develop: Deploy to Nexus') {
            sh 'mvn -P prod deploy -DskipTests=true'
        }

        return env
    } else {
        env = "DEV"

        stage('Develop: Deploy to Nexus') {
            sh 'mvn deploy -DskipTests=true'
        }

        return env
    }
}
