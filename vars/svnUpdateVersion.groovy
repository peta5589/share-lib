def call(Map project) {

    stage('Clone SVN repository'){
        sh "svn co ${project.svnURL} ${project.svnDir}"
    }

    stage('Add new version'){
        dir ("${project.svnDir}/${project.appName}"){
            app = sh (
                script: "ls -t | grep ${project.artifactId} | head -n1",
                returnStdout: true).trim()

            newVersionApp = app.split("-").first()+"-${project.version}"

            if(app){
                sh "cp ${app} ${newVersionApp}"
            } else {
                sh "cp ../<DEFAULT PROJECT NAME> ${newVersionApp}"
            }

        }
    }

    stage("Commit new version of ${project.artifactId} "){
        dir(project.svnDir){
            sh "svn add --force"
            sh "svn ci -m 'Add new version of ${project.artifactId}'"
        }
    }
}
