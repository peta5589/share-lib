def call(Map project){
    //Change old version in develop branch
    stage('Master: change old version in develop') {
        sh "git checkout ${project.branch}"
        sh "mvn versions:set -DnewVersion=${project.version}"
        sh "mvn versions:commit"

        newPom = readMavenPom file: 'pom.xml'
        sh "git add -A && git commit -m 'Skip build' && git push"
        sh "git tag -a VERSION_UPDATE -m 'Update version from ${project.vesion} to ${newPom.version}' && git push --tags"
    }
}
