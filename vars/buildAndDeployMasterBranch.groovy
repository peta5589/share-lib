def call(Map project) {

    if (project.releaseMaster) {
/*
         ucdImportNewVersion(
                ucdURL: project.ucdURL,
                application: project.appName,
                appProcess: project.appProcess,
                environment: project.environment,
                onlyChanged: project.onlyChanged,
                artifactId: project.artifactId,
                version: project.version)

        lastCommit = sh( returnStdout: true, script: 'git log -1 --pretty=%B')

        if (lastCommit.contains("[maven-release-plugin]")) {
            releaseDetected()
        } else {*/
        mavenRelease(profile: "prod")

        changeOldVersion(
            branch: "develop",
            version: project.version)

        svnUpdateVersion(
            svnURL: project.svnURL,
            svnDir: project.svnDir,
            appName: project.appName,
            version: project.version,
            artifactId: project.artifactId)

        ucdImportNewVersion(
               ucdURL: project.ucdURL,
               application: project.appName,
               appProcess: project.appProcess,
               environment: project.environment,
               onlyChanged: project.onlyChanged,
               artifactId: project.artifactId,
               version: newPom.version)
//        }

        environment = deployMasterToRef(project.deployMasterToRef)

        urbanCodeDeploy(
            ucdUrl: project.ucdUrl,
            artifactId: project.artifactId,
            onlyChanged: project.onlyChanged,
            version: project.version,
            environment: environment,
            appProcess: project.appProcess,
            appName: project.appName)
    }
}
