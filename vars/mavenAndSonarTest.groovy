def call(Map project) {
    stage('All: Unit-testing') {
        sh 'mvn test'
    }

    stage('All: Sonar-scanner') {
        withSonarQubeEnv('FenixSonarQube') {
            sh "mvn clean sonar:sonar " +
                    '-P sonar ' +
                    "-Dsonar.host.url=${project.hostUrl} " +
                    "-Dsonar.login=${project.login} " +
                    "-Dsonar.branch.name=${project.branchName} " +
                    "-Dsonar.projectName=${project.projectName} " +
                    "-Dsonar.projectKey=${project.projectKey} " +
                    "-Dsonar.java.source=${project.javaSource} " +
                    "-Dsonar.java.binaries=${project.javaBinaries} " +
                    "-Dsonar.language=${project.language} "
        }
    }
}