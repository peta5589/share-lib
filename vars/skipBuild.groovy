def call(){
    stage("Skip Build") {
        println "***********************************************"
        println "Last commit was modified by Jenkins tech. user."
        println "Jenkins will skip builds commited as 'skip build'"
        println "or when maven release plugin is used."
        println "***********************************************"
    }
}
